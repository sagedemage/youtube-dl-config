# youtube-dl-config

This is my config file for youtube dl.
## The flags that are setted
### Format the file to be .m4a audio type
* -f m4a
### Download only the audio files
* -x

## Config location in Linux and macOS
```
~/.config/youtube-dl/config
```
## Config location in Windows
```
C:\Users\<username>\youtube-dl.conf
```